# vim: ft=sls
{% from "package/map.jinja" import pkg_list with context %}

{%- if not pkg_list %}
'package_nothing_to_do':
  test.succeed_without_changes
{%- else %}
  {%- if 'present' in pkg_list %}
    {%- set os_family = salt['grains.get']('os_family', False)|lower %}
package_common_installed:
  pkg.installed:
    - pkgs:
    {%- for pkg in pkg_list.present %}
      - {{ pkg }}
    {%- endfor %}
    {%- if os_family == 'debian' %}
    - install_recommends: False
    {%- endif %}
  {%- endif %}

  {%- if 'absent' in pkg_list %}
package_common_removed:
  pkg.purged:
    - pkgs:
    {%- for pkg in pkg_list.absent %}
      - {{ pkg }}
    {%- endfor %}
  {%- endif %}
{%- endif %}
