# Saltstack state for packages managment

## Usage
Define a dictionary named 'package' in your pillar.

For dictionary structure, see pillar.example.

## Warning
You should use pillar for targeting the right operating system.

```
base:
  {#- get parameters #}
  {%- set os_family = salt['grains.get']('os_family', False)|lower %}

  # All minions get the following state files applied
  '*':
    - ignore_missing: True
    - {{os_family}}
```
whith debian.sls or redhat.sls

