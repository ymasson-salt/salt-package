def test_package_is_installed(host):
    package = host.package("iperf")
    assert package.is_installed

def test_package_is_purged(host):
    package = host.package("nano")
    assert not package.is_installed
